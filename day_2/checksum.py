import sys

file = open(sys.argv[1], "r")
# print(file.readline())
# print(file.readline())
# print(file.readline())

arrOfarr = []
for line in file : 
    temp = line
    arr = temp.split()
    arrOfarr.append(list(map(int, arr)))
checksum = []

# part 1
# for row in range(len(arrOfarr)) :
#     checksum.append(max(arrOfarr[row]) - min(arrOfarr[row]))

# part 2
for row in range(len(arrOfarr)) : 
    for col in range(len(arrOfarr[row])) : 
        curr = arrOfarr[row][col]
        for thing in range(len(arrOfarr[row])) :
            if curr % arrOfarr[row][thing] == 0 and curr != arrOfarr[row][thing]: 
                checksum.append(curr/arrOfarr[row][thing])
print(sum(checksum))